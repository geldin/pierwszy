    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <sys/wait.h>
    #include <signal.h>
    #include <time.h>
    #include <math.h>

#define TIME 1024 //protokol podajacy czas na serverze
#define SQUARE 1025  //protokol liczacy kwadrat liczb
#define LITTLEENDIAN 0  
#define BIGENDIAN 1  

int zamiana(int a);

int zamiana(int a){
	int new=0; 
				
	char *oldendian = (char *)&a; 		
	char *newendian = (char *)&new; 
	newendian[0] = oldendian[3]; 
	newendian[1] = oldendian[2]; 
	newendian[2] = oldendian[1]; 
	newendian[3] = oldendian[0]; 
						
				
return new;
}

int
main ()
{	
	time_t t;
        time(&t);

	int server_sockfd, client_sockfd;
	socklen_t server_len, client_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;

	server_sockfd = socket (AF_INET, SOCK_STREAM, 0);

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl (INADDR_ANY);
	server_address.sin_port = htons (9749);
	server_len = sizeof (server_address);
	bind (server_sockfd, (struct sockaddr *) &server_address, server_len);

	/*  Create a connection queue, ignore child exit details and wait for clients.  */

	listen (server_sockfd, 5);

	signal (SIGCHLD, SIG_IGN);

	//sprawdzanie edniana
	int i = 1;
	int endi;
    	char *p = (char *)&i;

 	if (p[0] == 1){
 	endi=LITTLEENDIAN;}//litle       	
    	else{
	endi=BIGENDIAN;}//big	
	//sprawdzanie endiana

	while (1)
	{        	

		int ch,a,b,c;

		printf ("server waiting\n");

		/*  Accept connection.  */

		client_len = sizeof (client_address);
		client_sockfd = accept (server_sockfd,
				(struct sockaddr *) &client_address,
				&client_len);

		/*  Fork to create a process for this client and perform a test to see
			whether we're the parent or the child.  */

		if (!fork())
		{

			/*  If we're the child, we can now read/write to the client on client_sockfd.
				The five second delay is just for this demonstration.  */
			for(;;){

				if((read(client_sockfd, &ch,sizeof(ch)))==-1){
					perror("odbieranie");
             				exit(1);
				}

				if(ch == TIME){
					write(client_sockfd, ctime(&t), 24);
				}else if(ch == SQUARE){
					if((read(client_sockfd, &a,sizeof(a)))==-1){
						perror("odbieranie");
	             				exit(1);
					}
					if((read(client_sockfd, &b,sizeof(b)))==-1){
						perror("odbieranie");
	             				exit(1);
					}
					if((read(client_sockfd, &c,sizeof(c)))==-1){
						perror("odbieranie");
	             				exit(1);
					}
					
					////////////////////////////////a^a
					int ile,ile2;
					float delta,x0,x1,x2;

					delta = ((float)b*(float)b) - (4*(float)a*(float)c);
					
					
					
					if(delta < 0){
						ile=0;
					}
					else if(delta == 0){	
						ile=1;				
					}
					else{
						ile=2;
					}
					
						
					if(endi==LITTLEENDIAN){
						ile= zamiana(ile);
					}

					write(client_sockfd, &ile, sizeof(ile));

					if(delta > 0){
						x1=(((float)b*(-1))-sqrt(delta))/(2*(float)a);
						write(client_sockfd, &x1, sizeof(x1));

						x2=(((float)b*(-1))+sqrt(delta))/(2*(float)a);
						write(client_sockfd, &x2, sizeof(x2));


					}
					else if(delta == 0){

						x0=((float)b*(-1))/(2*(float)a);
						write(client_sockfd, &x0, sizeof(x0));
						
					}else{
							//nic					
					}
					
				}         			
					
			}
			
			
		}

		/*  Otherwise, we must be the parent and our work for this client is finished.  */

		else
		{
			close (client_sockfd);
		}
	}
}
