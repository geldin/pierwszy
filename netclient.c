    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <sys/wait.h>
    #include <signal.h>
    #include <time.h>

#define TIME 1024 //protokol podajacy czas na serverze
#define SQUARE 1025  //protokol liczacy kwadrat liczb
#define LITTLEENDIAN 0  
#define BIGENDIAN 1  


int zamiana(int a);

int zamiana(int a){
	int new=0; 
				
	char *oldendian = (char *)&a; 		
	char *newendian = (char *)&new; 
	newendian[0] = oldendian[3]; 
	newendian[1] = oldendian[2]; 
	newendian[2] = oldendian[1]; 
	newendian[3] = oldendian[0]; 
							
return new;
}

int
main ()
{
	int sockfd;
	socklen_t len;
	struct sockaddr_in address;
	int result;
	int ch,a,b,c;

	/*  Create a socket for the client.  */

	sockfd = socket (AF_INET, SOCK_STREAM, 0);

	/*  Name the socket, as agreed with the server.  */

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr ("127.0.0.1");
	address.sin_port = htons (9749);
	len = sizeof (address);

	/*  Now connect our socket to the server's socket.  */

	result = connect (sockfd, (struct sockaddr *) &address, len);

	if (result == -1)
	{
		perror ("oops: netclient");
		exit (0);
	}
		
	//sprawdzanie edniana
	int i = 1;
	int endi;
    	char *p = (char *)&i;

 	if (p[0] == 1){
 	endi=LITTLEENDIAN;}//litle       	
    	else{
	endi=BIGENDIAN;}//big	
	//sprawdzanie endiana


	/*  We can now read/write via sockfd.  */
	for(;;){

		printf("nabazgraj co: ");
		scanf("%d",&ch);

		write(sockfd, &ch, sizeof(ch));

		if(ch == TIME){
			char czas[24]="";
			read(sockfd, &czas,24);
			printf ("time from server = %s\n", czas);
		}else if(ch == SQUARE){
				
			printf("liczba A : ");
			scanf("%d",&a);
			write(sockfd, &a, sizeof(a));

			printf("liczba B : ");
			scanf("%d",&b);
			write(sockfd, &b, sizeof(b));

			printf("liczba C : ");
			scanf("%d",&c);
			write(sockfd, &c, sizeof(c));
				
			int ile;
			float x0,x1,x2;
			
			read(sockfd, &ile,sizeof(ile));	
			
			
			if(endi==LITTLEENDIAN){
				ile = zamiana(ile);
			}				
		
				
			if(ile == 0){
				printf("delta ujemna, brak miejsc zerowych\n");
			}else if(ile == 1){
				read(sockfd, &x0,sizeof(x0));
				printf("jedno rozwiazanie : x0 = %f\n",x0);			
			}else if(ile == 2){
				read(sockfd, &x1,sizeof(x1));
				read(sockfd, &x2,sizeof(x2));
				printf("dwa rozwiazania : x1 = %f : x2 = %f \n", x1,x2);			
			}else{printf("Blad servera\n");}
				
			
		}

	}
	
	close (sockfd);
	exit (0);
}

//dopisac close i exit zero w obu plikach w poprawnym miejscu
